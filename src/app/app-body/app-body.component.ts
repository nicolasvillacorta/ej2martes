import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-app-body',
  templateUrl: './app-body.component.html',
  styleUrls: ['./app-body.component.css']
})
export class AppBodyComponent implements OnInit {

  vista = true;

  persona = {
    nombre: "Nicolas",
    apellido: "Villacorta",
    edad: 24
  }

  constructor() { }

  ngOnInit() {
  }

}
