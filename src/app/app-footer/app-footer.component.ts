import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-app-footer',
  templateUrl: './app-footer.component.html',
  styleUrls: ['./app-footer.component.css']
})
export class AppFooterComponent implements OnInit {

  fecha = new Date();

  titular = {
    nombre: "Nicolas",
    apellido: "Villacorta",

  }

  constructor() { }

  ngOnInit() {
  }

}
